using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TechTestPaymentApi.Entities
{
    public class Venda
    {
        [Key]
        public int Id { get; set; }
        
        
        [Required(ErrorMessage = "Data Venda é obrigatório")]
        public DateTime DataVenda { get; set; }


        [Required(ErrorMessage = "Itens é obrigatório")]
        [StringLength(100, MinimumLength = 2, ErrorMessage = "Itens deverá conter de 2 a 100 caracteres")]
        public string Itens { get; set; }


        [EnumDataType(typeof(EnumStatusVenda))]
        public EnumStatusVenda StatusVenda { get; set; }


        [Required(ErrorMessage = "Campo obrigatório")]
        [Range(1, 100, ErrorMessage = "Vendedor inválido")]
        public int VendedorId { get; set; }

        public Vendedor Vendedor { get; set; }
    }
}
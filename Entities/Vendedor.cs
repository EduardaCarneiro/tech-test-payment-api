using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TechTestPaymentApi.Entities
{
    public class Vendedor
    {
        [Key]
        public int Id { get; set; }
        
        
        [Required(ErrorMessage = "CPF é obrigatório")]
        [RegularExpression("^[0-9]{11}$", ErrorMessage = "CPF deve conter 11 digitos, apenas números são permitidos")]
        public string CPF { get; set; }

        
        [Required(ErrorMessage = "Nome é obrigatório")]
        [RegularExpression(".*[a-zA-Z]+.*", ErrorMessage = "Caracteres numéricos não são permitidos")]
        public string Nome { get; set; }

        
        [EmailAddress(ErrorMessage = "Email inválido")]
        [Required(ErrorMessage = "Email é obrigatório")]
        public string Email { get; set; }


        
        [Phone(ErrorMessage = "Telefone inválido")]     
        [RegularExpression("^[0-9]{11}$", ErrorMessage = "Telefone deve conter 11 números incluindo o DDD. Ex.:99000000000")]        
        [Required(ErrorMessage = "Telefone é obrigatório")]
        public string Telefone { get; set; }
    }
    
}
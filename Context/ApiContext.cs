using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using TechTestPaymentApi.Entities;


namespace TechTestPaymentApi.Context
{
    public class ApiContext : DbContext
    {
        public ApiContext(DbContextOptions<ApiContext> options) : base(options)
        {
            
        }

        public DbSet<Venda> Vendas { get; set;}
        public DbSet<Vendedor> Vendedores { get; set; }
    }
}
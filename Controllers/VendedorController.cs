using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TechTestPaymentApi.Context;
using TechTestPaymentApi.Entities;
using Microsoft.EntityFrameworkCore;

namespace TechTestPaymentApi.Controllers
{
    [ApiController]
    [Route("api-docs/[controller]/[action]")]
    
    public class VendedorController : ControllerBase
    {
        private readonly ApiContext _context;

        public VendedorController (ApiContext context)
        {
            _context = context;
        }


        /// <summary>
        /// Lista todos os vendedores cadastrados
        /// </summary>
        [HttpGet]
        public IActionResult ObterVendedoresCadastrados()
        {
            var vendedores = _context.Vendedores.ToList();
            return Ok(vendedores);
        }


        
        /// <summary>
        /// Cadastra novo vendedor
        /// </summary>
        [HttpPost]
        public IActionResult CadastrarVendedor(Vendedor vendedor)
        {
            if(ModelState.IsValid)
            {
                _context.Add(vendedor);
                _context.SaveChanges();
                return CreatedAtAction(nameof(ObterVendedoresCadastrados), new {id = vendedor.Id}, vendedor);
                
            }
            else
            {
                return BadRequest(ModelState);
            }
        }
    }
}
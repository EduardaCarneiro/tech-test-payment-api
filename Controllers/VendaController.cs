using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TechTestPaymentApi.Entities;
using TechTestPaymentApi.Context;

namespace TechTestPaymentApi.Controllers
{
    [ApiController]
    [Route("api-docs/[controller]/[action]")]
    
    public class VendaController : ControllerBase
    {
        private readonly ApiContext _context;

        public VendaController(ApiContext context)
        {
            _context = context;
        }

        

        /// <summary>
        /// Registra venda passando o Id do vendedor já cadastrado ou criando o vendedor na hora do registro
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /RegistrarVenda 
        ///     {                                                       
        ///        "id": 1,
        ///        "dataVenda": "2022-11-09",                                                 
        ///        "itens": "Item #1, Item #2, ...",                        
        ///        "statusVenda": "AguardandoPagamento",                    
        ///        "vendedorId": 3                                        
        ///     }
        ///
        ///     OU
        ///
        ///     POST /RegistrarVenda 
        ///     {
        ///       "id": 1,
        ///       "dataVenda": "2022-11-09",                                                 
        ///       "itens": "Item #1, Item #2, ...",
        ///       "statusVenda": "AguardandoPagamento",
        ///       "vendedorId": 100,
        ///       "vendedor": {
        ///         "id": 0,
        ///         "cpf": "00000000000",
        ///         "nome": "Nome",
        ///         "email": "user@example.com",
        ///         "telefone": "99000000000"
        ///       }
        ///     }
        /// </remarks>        
        [HttpPost]
        public IActionResult RegistrarVenda(Venda venda)
        {
                        
            if(venda.DataVenda == DateTime.MinValue || venda.DataVenda == null)
            {
                return BadRequest(new { Erro = "A data da venda é campo obrigatório" });
            }

            venda.StatusVenda = EnumStatusVenda.AguardandoPagamento;
            _context.Add(venda);
            _context.SaveChanges();
            return CreatedAtAction(nameof(ObterVendaPorId), new { id = venda.Id }, venda);
        }



        
        /// <summary>
        /// Busca venda por Id
        /// </summary>
        [HttpGet("{id}")]
        public IActionResult ObterVendaPorId(int id)
        {
            var venda = _context.Vendas.Find(id);            
            
            if(venda == null)
            {
                return NotFound();
            }


            var vendaCompleta = _context.Vendas.Include(x => x.Vendedor).Where(x => x.Id == id);            

            return Ok(vendaCompleta);
        }



        /// <summary>
        /// Atualiza apenas Status da Venda
        /// </summary>
        [HttpPut("{id}")]
        public IActionResult AtualizarStatusVenda(int id, Venda venda)
        {
            var vendaBanco = _context.Vendas.Find(id);

            if(vendaBanco == null)
            {
                return NotFound();
            }

            if(ChecarStatus(vendaBanco.StatusVenda, venda.StatusVenda) == false)
            {
                return BadRequest(new { Erro = $"Status da venda atual não pode ser atualizado para {venda.StatusVenda}!" });
            }          

            vendaBanco.StatusVenda = venda.StatusVenda;
            _context.Vendas.Update(vendaBanco);
            _context.SaveChanges();

            var statusAtualizado = _context.Vendas.Include(x => x.Vendedor).Where(x => x.Id == id);

            return Ok(statusAtualizado);
        }





        private bool ChecarStatus (EnumStatusVenda dbStaus, EnumStatusVenda novoStaus)
        {
            if(dbStaus == EnumStatusVenda.AguardandoPagamento)
            {
                if(novoStaus == EnumStatusVenda.PagamentoAprovado || novoStaus == EnumStatusVenda.Cancelada)
                {
                    return true;

                }                
            }
            else if (dbStaus == EnumStatusVenda.PagamentoAprovado)
            {
                if(novoStaus == EnumStatusVenda.EnviadoParaTransportadora || novoStaus == EnumStatusVenda.Cancelada)
                {
                    return true;

                }
            }
            else if(dbStaus == EnumStatusVenda.EnviadoParaTransportadora)
            {
                if(novoStaus == EnumStatusVenda.Entregue)
                {
                    return true;

                }
            }
            else
            {
                return false;
            }

            return false;
        } 

        
    }
}